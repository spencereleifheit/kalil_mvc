<?php
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Kalil Bottling.com!</title>

<style type="text/css">
<!--
a:link {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

a:hover {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#00F;
	text-decoration:none;
}

a:visited {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

html, body{
		width: 100%;
		height: 100%;
		margin: 0px auto;
		padding: 0;

		}
		#bg-css{
		position:absolute;
		height:100%;
		width: 100%;
		margin: 0px auto;
		padding: 0;
		z-index: -1;
		}

#background{
position:absolute;
height:100%;
width: 100%;
margin: 0;
padding: 0;
} 

#wrapper {
	
	position: relative; 
	width: 900px; 
	height: 980px;
	margin: 0px auto; 
	text-align: center;
	overflow: hidden;
    }

-->
</style>


<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>

<body>



<div>
  <img id = "bg-css" src = "mainbg.png" />
</div>

<div id = wrapper>


<div style = "position: absolute; left: 60px; top: 36px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "graybg.png" width="760" height="872"/>
</div>

<div style = "position: absolute; left: 64px; top: 33px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "whitebg.png" width="752" height="897"/>
</div>


<div style = "position: absolute; left: 80px; top: 80px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "historybanner.png" />
</div>


<div style = "position: absolute; left: 80px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
Home
</div>

<div style = "position: absolute; left: 140px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "kalil_products.html">
Products
</a>

</div>

<div style = "position: absolute; left: 225px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "how_we.html">
How We Make It
</a>
</div>


<div style = "position: absolute; left: 372px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "locations.html">
Locations
</a>
</div>

<div style = "position: absolute; left: 465px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "about_us.html">
About Us
</a>
</div>

<div style = "position: absolute; left: 555px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "company_history.html">
Company History
</a>
</div>

<div style = "position: absolute; left: 710px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "contact.html">
Contact Us
</a>
</div>

<div style = "position: absolute; left: 80px; top: 305px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 80px; top: 925px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 260px; top: 335px; font-family:'Verdana'; color: #000000; font-size: 22px; font-weight: bold; font-style: normal; z-index:-1;">
The History of a Family Business
</div>

<div style = "position: absolute; left: 290px; top: 365px; font-family:'Verdana'; color: #000000; font-size: 22px; font-weight: bold; font-style: normal; z-index:-1;">
Based on Principles and Pop
</div>

<div style = "position: absolute; left: 80px; top: 440px; font-family:'Verdana'; color: #000000; font-size: 12px; font-weight: bold; font-style: normal; z-index:-1;">
<img src = "fredphoto.png" />
</div>

<div style = "position: absolute; left: 120px; top: 742px; font-family:'Verdana'; color:#666 ; font-size: 12px; font-weight: normal; font-style: normal; z-index:-1;">
Founder Fred Kalil
</div>

<div style = "position: absolute; left: 295px; top: 440px; width: 520px; font-family:'Verdana'; color: #666; font-size: 14px; font-weight: bold; font-style: normal; z-index:-1; text-align: left;">

Fred Kalil and his father, Frank, founded Kalil Bottling Co. in Tucson in 1948.  Today, it's run by two of Fred's seven children, George and John.  Operations expanded into Phoenix in 1972, Northern Arizona in 1979 and El Paso in 1983.
<br /><br />

Kalil was the first bottler in the country to offer a carbonated, caffeine-free national brand cola, as well as a carbonated, sodium-free national brand cola.  In 1998, it was the first bottler in the U.S. to produce and distribute a national brand cola made with Splenda (sucralose).  In 1967, it became the second bottler in the Southwest to install a can line. 
<br /><br />

Today, as in 1948, Kalil Bottling Company continues to be an Arizona-based community-oriented bottler, involved each year in events both large and small, from cleanup campaigns to 4th of July fireworks celebrations.



</div>





</div>

</body>
</html>
