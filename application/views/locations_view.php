<?php

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Kalil Bottling.com!</title>

<style type="text/css">
<!--
a:link {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

a:hover {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#00F;
	text-decoration:none;
}

a:visited {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

html, body{
		width: 100%;
		height: 100%;
		margin: 0px auto;
		padding: 0;

		}
		#bg-css{
		position:absolute;
		height:100%;
		width: 100%;
		margin: 0px auto;
		padding: 0;
		z-index: -1;
		}

#background{
position:absolute;
height:100%;
width: 100%;
margin: 0;
padding: 0;
} 

#wrapper {
	
	position: relative; 
	width: 900px; 
	height: 1640px;
	margin: 0px auto; 
	text-align: center;
	overflow: hidden;
    }
-->
</style>


<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>

<body>



<div>
  <img id = "bg-css" src = "mainbg.png" />
</div>

<div id = "wrapper">


<div style = "position: absolute; left: 60px; top: 36px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "graybg.png" width="760" height="1500"/>
</div>

<div style = "position: absolute; left: 64px; top: 33px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "whitebg.png" width="752" height="1504"/>
</div>


<div style = "position: absolute; left: 80px; top: 80px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "locationsbanner.png" />
</div>


<div style = "position: absolute; left: 80px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "index.html">
Home
</a>
</div>

<div style = "position: absolute; left: 140px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "kalil_products.html">
Products
</a>

</div>

<div style = "position: absolute; left: 225px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "how_we.html">
How We Make It
</a>
</div>


<div style = "position: absolute; left: 372px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">

Locations

</div>

<div style = "position: absolute; left: 465px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "about_us.html">
About Us
</a>
</div>

<div style = "position: absolute; left: 555px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "company_history.html">
Company History
</a>
</div>

<div style = "position: absolute; left: 710px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "contact.html">
Contact Us
</a>
</div>

<div style = "position: absolute; left: 80px; top: 305px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 80px; top: 930px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 125px; top: 335px; width: 300px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">
<strong>
Corporate Office
</strong>
<br/>
<br />
931 South Highland Avenue
<br />
Tucson, Arizona 85719
<br />
(520) 624-1788
<br /><br />
<img src = "tucsonoffice.png" />
</div>

<div style = "position: absolute; left: 125px; top: 960px; width: 300px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">
<strong>
Flagstaff
</strong>
<br/>
<br />
1802 West Kaibab Lane #300
<br />
Flagstaff, Arizona 86001
<br />
(928) 779-2989
<br /><br />
<img src = "flagstaffoffice.png" />
</div>

<div style = "position: absolute; left: 460px; top: 960px; width: 300px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">
<strong>
El Paso
</strong>
<br/>
<br />
7328 Boeing Drive
<br />
El Paso, Texas 79925
<br />
(915) 778-4413
<br /><br />
<img src = "elpasooffice.png" />
</div>

<div style = "position: absolute; left: 460px; top: 335px; width: 300px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">
<strong>
Phoenix
</strong>
<br/>
<br />
2777 South Hardy Drive
<br />
Tempe, Arizona 85040
<br />
(480) 642-7777
<br /><br />
<img src = "phoenixoffice.png" />
</div>

<div style = "position: absolute; left: 125px; top: 660px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">

<iframe width="325" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=931+S+Highland+Ave+Tucson+AZ+85719&amp;aq=&amp;sll=32.211004,-110.950918&amp;sspn=0.01073,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=931+S+Highland+Ave,+Tucson,+Arizona+85719&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=32.211004,-110.950918&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=931+S+Highland+Ave+Tucson+AZ+85719&amp;aq=&amp;sll=32.211004,-110.950918&amp;sspn=0.01073,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=931+S+Highland+Ave,+Tucson,+Arizona+85719&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=32.211004,-110.950918" style="color:#0000FF;text-align:left">View Larger Map</a></small>

</div>

<div style = "position: absolute; left: 462px; top: 660px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">

<iframe width="325" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=2777+S+Hardy+Dr+Tempe+AZ&amp;aq=&amp;sll=31.791924,-106.387967&amp;sspn=0.010779,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=2777+S+Hardy+Dr,+Tempe,+Arizona+85284&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=33.398864,-111.951927&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=2777+S+Hardy+Dr+Tempe+AZ&amp;aq=&amp;sll=31.791924,-106.387967&amp;sspn=0.010779,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=2777+S+Hardy+Dr,+Tempe,+Arizona+85284&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=33.398864,-111.951927" style="color:#0000FF;text-align:left">View Larger Map</a></small>

</div>

<div style = "position: absolute; left: 80px; top: 1550px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 125px; top: 1280px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">

<iframe width="325" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=1802+West+Kaibab+Lane+%23300+Flagstaff+AZ&amp;aq=&amp;sll=32.211004,-110.950918&amp;sspn=0.01073,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=1802+W+Kaibab+Ln+%23300,+Flagstaff,+Arizona+86001&amp;t=m&amp;z=14&amp;ll=35.189903,-111.675295&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=1802+West+Kaibab+Lane+%23300+Flagstaff+AZ&amp;aq=&amp;sll=32.211004,-110.950918&amp;sspn=0.01073,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=1802+W+Kaibab+Ln+%23300,+Flagstaff,+Arizona+86001&amp;t=m&amp;z=14&amp;ll=35.189903,-111.675295" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</div>

<div style = "position: absolute; left: 462px; top: 1280px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: normal; font-style: normal; z-index:-1; text-align: center;">
<iframe width="325" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=7328+Boeing+Drive+El+Paso,+TX&amp;aq=&amp;sll=35.189903,-111.675295&amp;sspn=0.010364,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=7328+Boeing+Dr,+El+Paso,+Texas+79925&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=31.791924,-106.387967&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=7328+Boeing+Drive+El+Paso,+TX&amp;aq=&amp;sll=35.189903,-111.675295&amp;sspn=0.010364,0.023378&amp;ie=UTF8&amp;hq=&amp;hnear=7328+Boeing+Dr,+El+Paso,+Texas+79925&amp;t=m&amp;z=14&amp;iwloc=A&amp;ll=31.791924,-106.387967" style="color:#0000FF;text-align:left">View Larger Map</a></small>
</div>

</div>

</body>
</html>
