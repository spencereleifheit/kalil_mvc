<?php

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Kalil Bottling.com!</title>

<style type="text/css">
<!--
a:link {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

a:hover {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#00F;
	text-decoration:none;
}

a:visited {
	font:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#000;
	text-decoration:none;
}

html, body{
		width: 100%;
		height: 100%;
		margin: 0px auto;
		padding: 0;
		}
		#bg-css{
		position:absolute;
		height:100%;
		width: 100%;
		margin: 0px auto;
		padding: 0;
		z-index: -1;
		}

#background{
position:absolute;
height:100%;
width: 100%;
margin: 0;
padding: 0;
} 

#wrapper {
	
	position: relative; 
	width: 900px; 
	height: 1540px;
	margin: 0px auto; 
	text-align: center;
	overflow: hidden;
    }
-->
</style>


<script src="Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>

<body>



<div>
  <img id = "bg-css" src = "mainbg.png" />
</div>

<div id = wrapper>


<div style = "position: absolute; left: 60px; top: 36px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "graybg.png" width="760" height="1400"/>
</div>

<div style = "position: absolute; left: 64px; top: 33px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "whitebg.png" width="752" height="1404"/>
</div>


<div style = "position: absolute; left: 80px; top: 80px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
  <img src = "aboutusbanner.png" />
</div>


<div style = "position: absolute; left: 80px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href="index.html">
Home
</a>
</div>

<div style = "position: absolute; left: 140px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "kalil_products.html">
Products
</a>

</div>

<div style = "position: absolute; left: 225px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "how_we.html">
How We Make It
</a>
</div>


<div style = "position: absolute; left: 372px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "locations.html">
Locations
</a>
</div>

<div style = "position: absolute; left: 465px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
About Us
</div>

<div style = "position: absolute; left: 555px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "company_history.html">
Company History
</a>
</div>

<div style = "position: absolute; left: 710px; top: 50px; font-family:'Verdana'; color: #000000; font-size: 14px; font-weight: bold; font-style: normal; z-index:1;">
<a href = "contact.html">
Contact Us
</a>
</div>

<div style = "position: absolute; left: 80px; top: 305px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 80px; top: 1070px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 80px; top: 335px; width: 720px; font-family:'Verdana'; color: #666; font-size: 14px; font-weight: normal; font-style: normal; z-index:-1; text-align: left;">
<strong>A family-owned company since 1948, Kalil Bottling Co. </strong>is headquartered in Tucson, Arizona and has distribution centers in Phoenix, Flagstaff and El Paso.  Kalil distributes soft drinks, teas and waters throughout Arizona, parts of New Mexico, the Durango area of Colorado and the El Paso area of Texas.
<br /><br />
Major brands handled by the company's 830 employees include RC, 7-UP, Diet Rite, A&W, Sunkist, Eternal Water, Snapple, Arizona Tea, Canada Dry, Squirt, Vernors, YooHoo, Big Red, Fiji, Sun Drop, Sneaky Pete's, Monster Energy, Body Armor, Alkaline88 Water and More. 
<br /><br />
Kalil Bottling Co. offers a full line of fountain, vending and cold drink equipment, as well as beverage-related services.  Kalil also packs private-label soft drinks for customers throughout the Southwestern U.S. and has shipped soft drinks to customers in 14 different countries.
</div>

<div style = "position: absolute; left: 80px; top: 555px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

<div style = "position: absolute; left: 80px; top: 570px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
George Kalil, President
</div>

<div style = "position: absolute; left: 80px; top: 600px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<img src = "georgepic.png" />
</div>

<div style = "position: absolute; left: 305px; top: 600px; width:510px; font-family:'Verdana'; color: #666; font-size: 14px; font-weight: normal; font-style: normal; z-index:-1; text-align: left;">
George Kalil has been with Kalil Bottling Co. since the age of 10, when he got his start in the family business on Saturdays and during the summers by sweeping, sorting bottles, and placing bottles in the bottle-washing machine.  He assumed his first full-time job, delivering CO2 cylinders, when he was a sophomore in high school. 
<br /><br />
He has been president of the company since 1970, and under his leadership, net sales have increased from $400,000 to more than $190 million. 
<br /><br />
George is on the Board of Directors for the American Beverage Association, is the president of the Royal Crown Bottlers' Association, and is on the Board of Directors for the Seven Up Bottlers' Association. 
<br /><br />
He also is a founding member of the Arizona Beverage Industry Recycling Program. He was named the "Arizona Small Business Person of the Year" in 1981 and was selected Beverage Industry's 1998 "Executive of the Year." In 1991, the Retail Grocers' Association of Arizona named him "Supplier of the Year," and in 1992, he was
</div>

<div style = "position: absolute; left: 80px; top: 945px; width: 730px; font-family:'Verdana'; color: #666; font-size: 14px; font-weight: normal; font-style: normal; z-index:-1; text-align: left;">
elected to the Beverage Industry Hall of Fame.  In 1979,  the Exchange Clubs of Tucson selected him to receive the prestigious Golden Service Award.  He has also received numerous awards for community involvement. 
<br /><br />
George ensures that "The Good Guys at Kalil" is not just a company slogan, but also a working philosophy.  
</div>

<div style = "position: absolute; left: 80px; top: 1100px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
John Kalil, General Manager of Phoenix
</div>

<div style = "position: absolute; left: 80px; top: 1130px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<img src = "johnpic.png" />
</div>

<div style = "position: absolute; left: 305px; top: 1130px; width: 510px; font-family:'Verdana'; color: #666; font-size: 14px; font-weight: normal; font-style: normal; z-index:-1; text-align: left;">
Like George, John Kalil is a third generation member in a family business now employing fourth generation members. 
<br /><br />
A graduate of the University of Arizona and a native Arizonan now with four children, John's roots have always been in Arizona. 
<br /><br />
He is one of the original founders/ incorporators of both Phoenix Clean and Beautiful and Arizona Clean and Beautiful. 
<br /><br />
John is a current member and past president of the Arizona Soft Drink Association, and winner of the 1993 Beverage World Magazine "Green Award." 
<br /><br />
John moved to Phoenix from Tucson on July 2nd, 1975.  He now manages Kalil's newest distribution facility in Tempe, Arizona.  This facility currently distributes in a day what was more than a month's worth of business back in 1975 in the "Valley of the Sun."  
</div>

<div style = "position: absolute; left: 80px; top: 1460px; font-family:'Verdana'; color: #000000; font-size: 18px; font-weight: bold; font-style: normal; z-index:-1;">
<hr align = "center" size = "2" width="720">
</div>

</div>

</body>
</html>
