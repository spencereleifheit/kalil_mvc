<?php

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	//	Helper function to initialize Connection to the Website's DEVO Database

	function devo2Connect(){

		$serverName = "ecin1devo1sql4.ecbrands.com"; //serverName\instanceName
		$connectionInfo = array( "Database"=>"hspp1devo2", "UID"=>"hspp1awae1", "PWD"=>"h0r1z0n!");
		$pconn = sqlsrv_connect( $serverName, $connectionInfo);

		if( $pconn ) {
	    	echo "<h2>Connection established to DEVO 2(".$serverName.")</h2>";

	    	return $pconn;
		}

		else{
		     echo "Connection could not be established.<br />";
		     die( print_r( sqlsrv_errors(), true));
		     return false;
		}

	}