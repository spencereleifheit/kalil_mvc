<?php

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	//	Helper function to set today and the last 5 days for reporting purposes

	function goGetTheLastFiveDays(){

		$arrayOfSixDays = array();
		
		date_default_timezone_set('America/Phoenix');

		$date = date('Y-m-d', time());

		$time_append = " 00:00:00.000";

		$five_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 5, date("Y")));
		$four_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 4, date("Y")));	
		$three_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 3, date("Y")));
		$two_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 2, date("Y")));
		$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));

		$arrayOfSixDays[0] = $date;
		$arrayOfSixDays[1] = $yesterday;
		$arrayOfSixDays[2] = $two_days_ago;
		$arrayOfSixDays[3] = $three_days_ago;
		$arrayOfSixDays[4] = $four_days_ago;
		$arrayOfSixDays[5] = $five_days_ago;

		return $arrayOfSixDays;

	}