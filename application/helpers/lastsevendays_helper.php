<?php

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	//	Helper function to set today and the last 7 days before today for reporting purposes

	function goGetTheLastSevenDays(){

		$arrayOfEightDays = array();
		
		date_default_timezone_set('America/Phoenix');

		$date = date('Y-m-d', time());

		$time_append = " 00:00:00.000";

		$seven_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 7, date("Y")));
		$six_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 6, date("Y")));
		$five_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 5, date("Y")));
		$four_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 4, date("Y")));	
		$three_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 3, date("Y")));
		$two_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 2, date("Y")));
		$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));

		$arrayOfEightDays[0] = $date;
		$arrayOfEightDays[1] = $yesterday;
		$arrayOfEightDays[2] = $two_days_ago;
		$arrayOfEightDays[3] = $three_days_ago;
		$arrayOfEightDays[4] = $four_days_ago;
		$arrayOfEightDays[5] = $five_days_ago;
		$arrayOfEightDays[6] = $six_days_ago;
		$arrayOfEightDays[7] = $seven_days_ago;

		return $arrayOfEightDays;

	}